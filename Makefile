default: paratest

init:
	tests/bin/console doctrine:schema:drop --force
	tests/bin/console doctrine:schema:create

paratest: init
	./../../vendor/bin/paratest --colors

phpunit: init
	./../../vendor/bin/phpunit -d --update-snapshots
