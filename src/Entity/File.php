<?php

declare(strict_types=1);

namespace UXF\Storage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_storage')]
#[ORM\InheritanceType(value: 'SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'file_type', type: 'string')]
#[ORM\DiscriminatorMap(value: [
    'image' => Image::class,
    'file' => File::class,
])]
class File
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id = 0;

    #[ORM\Column(type: 'uuid', unique: true)]
    private UuidInterface $uuid;

    #[ORM\Column]
    private string $extension;

    #[ORM\Column]
    private string $type;

    #[ORM\Column]
    private string $name;

    #[ORM\Column]
    private string $namespace;

    #[ORM\Column]
    private int $size;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $createdAt;

    public function __construct(
        UuidInterface $uuid,
        string $extension,
        string $type,
        string $name,
        string $namespace,
        int $size,
    ) {
        $this->uuid = $uuid;
        $this->extension = $extension;
        $this->type = $type;
        $this->name = $name;
        $this->namespace = $namespace;
        $this->size = $size;
        $this->createdAt = Clock::now();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getFilename(): string
    {
        return "{$this->uuid}.{$this->extension}";
    }

    public function getPath(): string
    {
        // BC namespace
        $id = $this->uuid->toString();
        return "/$this->namespace/$id[0]/$id[1]/{$this->getFilename()}";
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
