<?php

declare(strict_types=1);

namespace UXF\Storage\Http\Response;

use UXF\Hydrator\Attribute\HydratorMap;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;

#[HydratorMap('fileType', [
    'file' => FileResponse::class,
    'image' => ImageResponse::class,
])]
abstract readonly class StorageResponse
{
    public static function from(File $file): StorageResponse
    {
        if ($file instanceof Image) {
            return new ImageResponse(
                id: $file->getId(),
                uuid: $file->getUuid(),
                type: $file->getType(),
                extension: $file->getExtension(),
                name: $file->getName(),
                namespace: $file->getNamespace(),
                width: $file->getWidth(),
                height: $file->getHeight(),
                size: $file->getSize(),
                createdAt: $file->getCreatedAt(),
            );
        }

        return new FileResponse(
            id: $file->getId(),
            uuid: $file->getUuid(),
            type: $file->getType(),
            extension: $file->getExtension(),
            name: $file->getName(),
            namespace: $file->getNamespace(),
            size: $file->getSize(),
            createdAt: $file->getCreatedAt(),
        );
    }

    public static function fromNullable(?File $file): ?StorageResponse
    {
        return $file === null ? null : self::from($file);
    }
}
