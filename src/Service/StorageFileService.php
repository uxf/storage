<?php

declare(strict_types=1);

namespace UXF\Storage\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Storage\Entity\File;
use UXF\Storage\Exception\CreateFileException;
use UXF\Storage\Service\FileCreator\FileCreator;

final readonly class StorageFileService
{
    /**
     * @param FileCreator[] $fileCreators
     */
    public function __construct(
        private iterable $fileCreators,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function createFile(mixed $uploadedFile, string $namespace): File
    {
        $file = null;
        foreach ($this->fileCreators as $fileCreator) {
            $file = $fileCreator->createFile($uploadedFile, $namespace);
            if ($file !== null) {
                break;
            }
        }

        if ($file === null) {
            throw new CreateFileException();
        }

        $this->entityManager->persist($file);
        $this->entityManager->flush();
        return $file;
    }
}
