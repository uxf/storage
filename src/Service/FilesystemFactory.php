<?php

declare(strict_types=1);

namespace UXF\Storage\Service;

use AsyncAws\Core\Configuration;
use AsyncAws\S3\S3Client;
use AzureOss\FlysystemAzureBlobStorage\AzureBlobStorageAdapter;
use AzureOss\Storage\Blob\BlobContainerClient;
use AzureOss\Storage\Common\Auth\StorageSharedKeyCredential;
use GuzzleHttp\Psr7\Uri;
use InvalidArgumentException;
use League\Flysystem\AsyncAwsS3\AsyncAwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use League\Flysystem\Visibility;
use LogicException;
use UXF\Core\Type\Url;
use UXF\Core\Type\UrlComponents;

final readonly class FilesystemFactory
{
    public static function createFilesystem(string $dsn): FilesystemOperator
    {
        $config = Url::parseWithoutDomainCheck($dsn)->getComponents();

        $adapter = match ($config->scheme) {
            'local' => self::createLocalAdapter($config),
            'aws', 's3' => self::createAwsAdapter($config),
            'azure' => self::createAzureAdapter($config),
            default => throw new InvalidArgumentException("Invalid storage schema '{$config->scheme}'"),
        };

        return new Filesystem($adapter, [
            'visibility' => 'public',
        ]);
    }

    // local://default/dir
    private static function createLocalAdapter(UrlComponents $config): FilesystemAdapter
    {
        $directory = substr($config->path ?? throw new InvalidArgumentException('Missing path'), 1);

        return new LocalFilesystemAdapter($directory, new PortableVisibilityConverter(defaultForDirectories: Visibility::PUBLIC));
    }

    // s3://key:secret@domain/bucket
    private static function createAwsAdapter(UrlComponents $config): FilesystemAdapter
    {
        if (!class_exists(AsyncAwsS3Adapter::class)) {
            throw new LogicException('Please call: composer req league/flysystem-async-aws-s3');
        }

        [$httpSchema, $host, $accessKeyId, $accessKeySecret, $bucket] = self::parseUrl($config);

        $s3Client = new S3Client(Configuration::create([
            Configuration::OPTION_ACCESS_KEY_ID => $accessKeyId,
            Configuration::OPTION_SECRET_ACCESS_KEY => $accessKeySecret,
            Configuration::OPTION_ENDPOINT => "$httpSchema://$host",
            Configuration::OPTION_PATH_STYLE_ENDPOINT => 'true',
        ]));

        return new AsyncAwsS3Adapter($s3Client, $bucket);
    }

    // azure://accountName:accountKey@core.windows.net/container
    private static function createAzureAdapter(UrlComponents $config): FilesystemAdapter
    {
        if (!class_exists(AzureBlobStorageAdapter::class)) {
            throw new LogicException('Please call: composer req azure-oss/storage-blob-flysystem');
        }

        [$httpSchema, $endpointSuffix, $accountName, $accountKey, $container] = self::parseUrl($config);

        return new AzureBlobStorageAdapter(new BlobContainerClient(
            new Uri("$httpSchema://$accountName.blob.$endpointSuffix/$container"),
            new StorageSharedKeyCredential($accountName, $accountKey),
        ));
    }

    /**
     * @return array{string,string,string,string,string}
     */
    private static function parseUrl(UrlComponents $url): array
    {
        return [
            $url->query['schema'] ?? 'https',
            $url->host ?? throw new InvalidArgumentException("Missing host"),
            $url->user ?? throw new InvalidArgumentException("Missing user"),
            $url->pass ?? throw new InvalidArgumentException("Missing pass"),
            substr($url->path ?? throw new InvalidArgumentException("Missing path"), 1), // drop leading slash
        ];
    }
}
