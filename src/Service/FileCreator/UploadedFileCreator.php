<?php

declare(strict_types=1);

namespace UXF\Storage\Service\FileCreator;

use League\Flysystem\FilesystemOperator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\MimeTypesInterface;
use UXF\Core\SystemProvider\Uuid;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;
use UXF\Storage\Utils\DestinationDirHelper;

final readonly class UploadedFileCreator implements FileCreator
{
    public function __construct(
        private FilesystemOperator $defaultFilesystem,
        private MimeTypesInterface $mimeTypes,
    ) {
    }

    public function createFile(object $file, string $namespace): ?File
    {
        if (!$file instanceof UploadedFile) {
            return null;
        }

        $uuid = Uuid::uuid4();

        $type = $file->getClientMimeType();
        $ext = $file->getClientOriginalExtension();
        $ext = $ext !== '' ? $ext : ($this->mimeTypes->getExtensions($type)[0] ?? 'jpg');
        $ext = mb_strtolower($ext);

        $size = $file->getSize();
        $raw = $file->getContent();

        $destDir = DestinationDirHelper::getDir($uuid, $namespace);
        $this->defaultFilesystem->write("$destDir/$uuid.$ext", $file->getContent());

        $originalName = $file->getClientOriginalName();

        if (str_starts_with($type, 'image/')) {
            $result = @getimagesizefromstring($raw);
            [$width, $height] = $result !== false ? $result : [0, 0];
            return new Image($uuid, $ext, $type, $originalName, $namespace, $size, $width, $height);
        }

        return new File(
            $uuid,
            $ext,
            $type,
            $originalName,
            $namespace,
            $size,
        );
    }
}
