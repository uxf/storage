<?php

declare(strict_types=1);

namespace UXF\Storage\Service\FileCreator;

use League\Flysystem\FilesystemOperator;
use RuntimeException;
use Symfony\Component\Mime\MimeTypesInterface;
use UXF\Core\SystemProvider\Uuid;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;
use UXF\Storage\Http\Request\FileRequestBody;
use UXF\Storage\Utils\Base64Helper;
use UXF\Storage\Utils\DestinationDirHelper;

final readonly class RequestFileCreator implements FileCreator
{
    public function __construct(
        private FilesystemOperator $defaultFilesystem,
        private MimeTypesInterface $mimeTypes,
    ) {
    }

    public function createFile(object $file, string $namespace): ?File
    {
        if (!$file instanceof FileRequestBody) {
            return null;
        }

        $originalName = $file->name ?? 'file.jpg';
        $type = $file->type ?? 'image/jpeg';

        $uuid = Uuid::uuid4();
        $ext = pathinfo($originalName, PATHINFO_EXTENSION);
        if ($ext === '') {
            $ext = $this->mimeTypes->getExtensions($type)[0] ?? 'jpg';
        }
        $ext = mb_strtolower($ext);

        // fix weird formats
        if ($ext === 'heic') {
            $ext = 'jpg';
        }

        $raw = Base64Helper::decode($file->content);

        $destDir = DestinationDirHelper::getDir($uuid, $namespace);

        $filename = "$destDir/$uuid.$ext";
        $this->defaultFilesystem->write($filename, $raw);

        $size = $this->defaultFilesystem->fileSize($filename);
        if ($size === 0) {
            throw new RuntimeException("Zero file size '$filename'");
        }

        if (str_starts_with($type, 'image/')) {
            $result = @getimagesizefromstring($raw);
            [$width, $height] = $result !== false ? $result : [0, 0];
            return new Image($uuid, $ext, $type, $originalName, $namespace, $size, $width, $height);
        }

        return new File($uuid, $ext, $type, $originalName, $namespace, $size);
    }
}
