<?php

declare(strict_types=1);

namespace UXF\Storage\Service\FileCreator;

use UXF\Storage\Entity\File;

interface FileCreator
{
    public function createFile(object $file, string $namespace): ?File;
}
