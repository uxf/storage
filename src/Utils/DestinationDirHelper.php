<?php

declare(strict_types=1);

namespace UXF\Storage\Utils;

use Ramsey\Uuid\UuidInterface;

final readonly class DestinationDirHelper
{
    public static function getDir(UuidInterface $uuid, string $namespace): string
    {
        $id = $uuid->toString();
        return "/$namespace/$id[0]/$id[1]";
    }
}
