<?php

declare(strict_types=1);

namespace UXF\StorageTests\Unit;

use AzureOss\FlysystemAzureBlobStorage\AzureBlobStorageAdapter;
use League\Flysystem\AsyncAwsS3\AsyncAwsS3Adapter;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\Local\LocalFilesystemAdapter;
use PHPUnit\Framework\TestCase;
use ReflectionObject;
use UXF\Storage\Service\FilesystemFactory;

class FilesystemFactoryTest extends TestCase
{
    public function testLocal(): void
    {
        $service = FilesystemFactory::createFilesystem('local://default/dir');
        $adapter = self::getAdapter($service);
        self::assertInstanceOf(LocalFilesystemAdapter::class, $adapter);
    }

    public function testAws(): void
    {
        $service = FilesystemFactory::createFilesystem('aws://key:secret@domain/bucket?schema=http');
        $adapter = self::getAdapter($service);
        self::assertInstanceOf(AsyncAwsS3Adapter::class, $adapter);
    }

    public function testAzure(): void
    {
        $service = FilesystemFactory::createFilesystem('azure://accountName:accountKey@core.windows.net/container');
        $adapter = self::getAdapter($service);
        self::assertInstanceOf(AzureBlobStorageAdapter::class, $adapter);
    }

    private static function getAdapter(FilesystemOperator $filesystem): FilesystemAdapter
    {
        $ref = new ReflectionObject($filesystem);
        $p = $ref->getProperty('adapter');
        $p->setAccessible(true);
        return $p->getValue($filesystem);
    }
}
