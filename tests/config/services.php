<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Storage\Service\StorageFileService;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->public();

    $services->set(StorageFileService::class)
        ->arg('$fileCreators', tagged_iterator('uxf.storage.file_creator'));

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
        ],
    ]);

    $containerConfigurator->extension('uxf_storage', [
        'filesystems' => [
            'default' => 'local://default/%kernel.project_dir%/tests/public/upload',
        ],
    ]);
};
