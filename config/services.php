<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Storage\Service\FileCreator\RequestFileCreator;
use UXF\Storage\Service\FileCreator\UploadedFileCreator;
use UXF\Storage\Service\StorageFileService;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(StorageFileService::class)
        ->arg('$fileCreators', tagged_iterator('uxf.storage.file_creator'));

    $services->set(RequestFileCreator::class);
    $services->set(UploadedFileCreator::class);
};
